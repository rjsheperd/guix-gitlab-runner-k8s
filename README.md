Guix GitLab Runner
==================

This project provides Helm chart configuration values for deploying a [GitLab Runner](https://docs.gitlab.com/runner/) on a
Kubernetes cluster enabled for running workloads against a persistent Guix installation. Once registered, the Runner will
then be available to tagged GitLab CI/CD jobs which require Guix.

Installation
------------

### Prerequisites ###

* [Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) on your development machine.
* [Install Helm](https://helm.sh/docs/intro/quickstart/) on your development machine.
* [Install Guix](https://gitlab.com/singularsyntax/guix-k8s) on your K8s cluster.

### Registration Token ###

Instructions for obtaining the token required to register your runner with a group or project on <https://gitlab.com/> are
provided [here](https://docs.gitlab.com/runner/register/#requirements). The token will look something like `abcdeFGHIJ0123456789`
and must be Base64 encoded and set in `00-gitlab-runner-secret.yaml`:

    token=`echo -n "abcdeFGHIJ0123456789" | base64`; sed "s/<base64-encoded-token>/$token/" 00-gitlab-runner-secret.yaml

This results in something like:

    ---
    apiVersion: v1
    kind: Secret
    metadata:
      name: gitlab-runner-secret
    type: Opaque
    data:
      runner-registration-token: "YWJjZGVGR0hJSjAxMjM0NTY3ODk="
      runner-token: ""

You can then create the registration secret. You should use `guix-gitlab-runner` for `<namespace>` unless you used a different
one when you installed Guix on the cluster:

    kubectl apply -f 00-gitlab-runner-secret.yaml -n <namespace>

### Install and Register Runner ###

Instructions for installing the GitLab Runner Helm chart are documented [here](https://docs.gitlab.com/runner/install/kubernetes.html),
but the defaults provided in `values.yaml` should be sufficient for most uses. Just execute:

    helm install --namespace guix-gitlab-runner guix -f values.yaml gitlab/gitlab-runner --version 0.19.2

Usage
-----

In `.gitlab-ci.yml` jobs, simply use a Docker image which includes a `guix` command binary (such as [singularsyntax/guix-client](https://hub.docker.com/r/singularsyntax/guix-client)),
and include the `guix` tag so that the Guix Runner selects the job.

The Runner will set the [`GUIX_DAEMON_SOCKET`](https://guix.gnu.org/manual/en/html_node/The-Store.html) environment variable to
`guix://guix-daemon` on containers it creates for the job, so any `guix` command invocations in the job script will run against
the cluster Guix daemon seamlessly.

Here is an example:

    build:
      stage: build
      image: singularsyntax/guix-client:1.1.0
      script:
        - guix pack --format=tarball --symlink=/bin=bin hello
      tags:
        - guix
